<?php

function search($whereToSearch, $whatToSearch)
{
    $out = [];
    foreach ($whereToSearch as $key => $string) {
        $var = [];
        foreach ($whatToSearch as $word) {
            if (preg_match_all('/' . $word . '/iu', $string)) {
                $var[] = $word;
            }
        }
        $data = implode(', ', $var);
        $out[] = 'В предложении № ' . ($key + 1) . ' найдены слова : ' . $data . '.' . '<br>';
    }
    return $out;
}